import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'comanda',
    loadChildren: () => import('./pages/comanda/comanda.module').then( m => m.ComandaPageModule)
  },
  {
    path: 'vendedores',
    loadChildren: () => import('./pages/vendedores/vendedores.module').then( m => m.VendedoresPageModule)
  },
  {
    path: 'clientes',
    loadChildren: () => import('./pages/clientes/clientes.module').then( m => m.ClientesPageModule)
  },  {
    path: 'ordenes',
    loadChildren: () => import('./pages/ordenes/ordenes.module').then( m => m.OrdenesPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
