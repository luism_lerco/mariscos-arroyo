export interface ProductosInterface{
    id: any;
    clave: any;
    avatar: any;
    nombre: any;
    text: any;
    descripcion: any;
    is_subproducto: any;
    sub_cantidad_equivalente: any;
    sub_producto_id: any;
    sub_producto_nombre: any;
    sub_existencia: any;
    tipo: any;
    existencia: any;
    tipo_medida: any;
    categoria: any;
    proveedor: any;
    costo: any;
    precio_publico: any;
    precio_mayoreo: any;
    precio_menudeo: any;
    precio_venta: any;
    tipo_venta: any;
}