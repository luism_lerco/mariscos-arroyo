export interface ResponseInterface{
    status:string;
    code:string;
    message:any;
    data:any;
    result:any;
    vendedores:any;
    clientes: any;
    producto: any;
}