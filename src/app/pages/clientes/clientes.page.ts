import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ResponseInterface } from 'src/app/modelos/response.interface';
import { ClientesInterface } from 'src/app/modelos/clientes.interface';

import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {

  //iniciamos variable para la busqueda
  nom_cliente = '';

  constructor(private api:ApiService, private modalCtrl:ModalController) { }

  //Inicializamos nuestro arreglo para las peticiones
  public usr_search = {
    token: '',
    search_cliente: ''
  }

  //Arreglo lista clientes
  public clientesList: any[] = [];


  //Declaramos las interfaces a usar
  clientes!: ClientesInterface;

  ngOnInit() {
    //Obtenemos el token y lo asignamos 
    let toke = localStorage.getItem('token');
    this.usr_search.token = toke;
  }

  buscarCliente(event: CustomEvent){
    //Obetenemos el valor del buscador
    this.nom_cliente = event.detail.value;
    //Console log de control
    //console.log(this.nom_cliente);
    //Asignamos el nombre del cliente a la peticion
    this.usr_search.search_cliente = this.nom_cliente;
    //console.log de control
    //console.log(this.usr_search);
    this.api.getClientes(this.usr_search).subscribe(data =>{
      let dataResponse: ResponseInterface = data;
      this.clientes = dataResponse.clientes;
      //Asignamos al Array la lista de vendedores
      this.clientesList = Array.from(dataResponse.clientes);
      //console.log de control
      //console.table(this.clientesList);
    });

  }

  selec_cliente(cliente:any){
    this.modalCtrl.dismiss({
      id: cliente.id,
      cliente: cliente.nombre
    })
  }

  publicoGral(){
    this.modalCtrl.dismiss({
      id: 3,
      cliente: 'Publico en Gral'
    });
  }

}
