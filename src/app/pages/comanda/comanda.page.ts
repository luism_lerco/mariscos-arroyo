import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { VendedoresPage } from '../vendedores/vendedores.page';
import { ClientesPage } from '../clientes/clientes.page';
import { OrdenesPage } from '../ordenes/ordenes.page';

import { ResponseInterface } from 'src/app/modelos/response.interface';
import { VendedoresInterface} from 'src/app/modelos/vendedores.interface';

import { AlertController } from '@ionic/angular';

import { Router } from '@angular/router';
import { ApiService } from 'src/app/servicios/api/api.service';


@Component({
  selector: 'app-comanda',
  templateUrl: './comanda.page.html',
  styleUrls: ['./comanda.page.scss'],
})
export class ComandaPage implements OnInit {

  //public misInputs: any[] = [];

  public comanda = {
    "token" : '' ,
    "cliente_id" : '',
    "total": '',
    "tipo": '',
    "nombre_vendedor": '',
    "nombre_cliente":'',
    "venta_detalle": []
  }

  @Input() nombre_vendedor: string;
  @Input() token_vendedor: string;
  @Input() id_cliente: number;
  @Input() nombre_cliente: string;

  constructor(private router:Router, private api:ApiService, private alertCtrl:AlertController, private modalCtrl:ModalController) {

    //this.misInputs = this.crearInputs();

   }

  ngOnInit() {

    this.checkLocalStorage();

  }


  async addOrders() {
    const modal_vend = await this.modalCtrl.create({
      component: VendedoresPage,
      cssClass: 'my-custom-class',
      backdropDismiss: false
    });

    await modal_vend.present();

    const resp_vend = await modal_vend.onWillDismiss();
    console.log(resp_vend.data.nombre);
    let vend_token = resp_vend.data.token;
    let vend_nomb = resp_vend.data.nombre;

    const modal_cliente = await this.modalCtrl.create({
      component: ClientesPage,
      cssClass: 'my-custom-class',
      backdropDismiss: false
    });

    await modal_cliente.present();

    const resp_cliente = await modal_cliente.onWillDismiss();
    //console.log de control
    //console.log(resp_cliente.data.cliente);
    let client_nom = resp_cliente.data.cliente;
    let client_id = resp_cliente.data.id; 

    const modal_orden = await this.modalCtrl.create({
      component: OrdenesPage,
      backdropDismiss: false,
      cssClass: 'orden-modal',
      componentProps: {
        'nombre_vendedor': vend_nomb,
        'token_vendedor': vend_token,
        'id_cliente': client_id,
        'nombre_cliente': client_nom,
      }
    });

    await modal_orden.present();
  }


  //cerrar sesion
  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(['login']);
  }

  checkLocalStorage(){
    //si regresamos a la pagina de login y el token existe entonces redirigimos al usuario al inicio del dash
    if (localStorage.getItem('token')) {
      this.router.navigate(['comanda']);
    }
  }

}
