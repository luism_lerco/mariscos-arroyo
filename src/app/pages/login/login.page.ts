import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ApiService } from 'src/app/servicios/api/api.service';
import { ResponseInterface } from 'src/app/modelos/response.interface';
import { LoginInterface } from 'src/app/modelos/login.interface';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm = new FormGroup({
    username : new FormControl('', Validators.required),
    password : new FormControl('', Validators.required)
  })

  public msg_error ='';

  constructor(public toastController: ToastController, private api:ApiService, private router:Router) { 


  }

  ngOnInit() {

    this.checkLocalStorage();
    
  }

  onLogin(form:LoginInterface){
    console.log(form);
    this.api.loginByEmail(form).subscribe(data =>{
      console.log(data)
      let dataResponse:ResponseInterface = data;
      if(dataResponse.code == '202'){
        localStorage.setItem("token", dataResponse.data.token);
        this.loginOK();
        this.router.navigate(['comanda']);
      }
      if(dataResponse.code == '10'){
        this.msg_error = dataResponse.message;
        this.loginErr();
      }
    });
  }

  checkLocalStorage(){
    //si regresamos a la pagina de login y el token existe entonces redirigimos al usuario al inicio del dash
    if (localStorage.getItem('token')) {
      this.router.navigate(['comanda']);
    }
  }

  async loginOK() {
    const toast = await this.toastController.create({
      message: 'Bienvenido!.',
      color: 'success',
      icon: 'checkmark-outline',
      duration: 1000
    });
    toast.present();
  }

  async loginErr() {
    const toast = await this.toastController.create({
      message: this.msg_error,
      color: 'danger',
      icon: 'alert-circle-outline',
      duration: 2400
    });
    toast.present();
  }

}
