import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ResponseInterface } from 'src/app/modelos/response.interface';
import { ProductosInterface} from 'src/app/modelos/productos.interface';

import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-ordenes',
  templateUrl: './ordenes.page.html',
  styleUrls: ['./ordenes.page.scss'],
})
export class OrdenesPage implements OnInit {

  //iniciamos variable para la busqueda
  nom_prod = '';

  public venta_detalle:any = [
		{
			producto_id : 0,
			cantidad    : 0,
			precio_venta: 0
		}
	]

  constructor(private api:ApiService, private modalCtrl:ModalController) { }

  //Inicializamos nuestro arreglo para las peticiones
  public prod_search = {
    token: '',
    producto_name: ''
  }

  //Arreglo lista clientes
  public productosList: any[] = [];


  //Declaramos las interfaces a usar
  productos!: ProductosInterface;

  ngOnInit() {
    //Obtenemos el token y lo asignamos 
    let toke = localStorage.getItem('token');
    this.prod_search.token = toke;

  }

  buscarProducto( event: CustomEvent ){
     //Obetenemos el valor del buscador
     this.nom_prod = event.detail.value;
     //Console log de control
    //console.log(this.nom_prod);
    //Asignamos el nombre del producto a la peticion
    this.prod_search.producto_name = this.nom_prod
    this.api.getProductos(this.prod_search).subscribe(data =>{
      let dataResponse: ResponseInterface = data;
      this.productos = dataResponse.producto;
      //Asignamos al Array la lista de vendedores
      //this.productosList = Array.from(dataResponse.producto);
      this.productosList = Array.from(dataResponse.producto);
      //console.log de control
      //console.table(this.clientesList);
    });
  }

  cerrarOrden(){
    this.modalCtrl.dismiss();
  }

  add_producto(producto:any){
    console.log(producto.nombre);
    console.log(producto.precio_venta);
    console.log(producto.id);
  }

}
