import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { ResponseInterface } from 'src/app/modelos/response.interface';
import { VendedoresInterface } from 'src/app/modelos/vendedores.interface';

import { ApiService } from 'src/app/servicios/api/api.service';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.page.html',
  styleUrls: ['./vendedores.page.scss'],
})
export class VendedoresPage implements OnInit {

  constructor(private api:ApiService, private modalCtrl:ModalController) { }

   //Inicializamos nuestro arreglo para las peticiones
   public vend_srch = {
    token: '',
  }
  
  //nomb_vendedor = '';
  //toke_vendedor = '';

  //Arreglo lista vendedores
  public vendedoresList: any[] = [];

  //Declaramos las interfaces a usar
  vendedores!: VendedoresInterface;

  ngOnInit() {
    //Obtenemos el token y lo asignamos 
    let toke = localStorage.getItem('token');
    this.vend_srch.token = toke;

    this.api.getVendedores(this.vend_srch).subscribe(data => {
      let dataResponse: ResponseInterface = data;
      this.vendedores = dataResponse.vendedores;
      //Asignamos al Array la lista de vendedores
      this.vendedoresList = Array.from(dataResponse.vendedores);
      //console.log de control
      //console.table(this.vendedoresList);
    })

  }

  selec_vend(vendedor:any){
    
    this.modalCtrl.dismiss({
      nombre: vendedor.nombre,
      token_vendedor: vendedor.token
    }); 
  }

}
