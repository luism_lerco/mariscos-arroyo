import { Injectable } from '@angular/core';

import { ResponseInterface } from 'src/app/modelos/response.interface';
import { LoginInterface } from 'src/app/modelos/login.interface';

import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  url:string = "http://demo-arroyo.lerco.agency/web/v1/auth/";
  url2:string = "http://demo-arroyo.lerco.agency/web/v1/user/";
  url3:string = "http://demo-arroyo.lerco.agency/web/v1/";

  constructor(private http:HttpClient) { }

  headers: HttpHeaders = new HttpHeaders({
    "Content-Type": "application/json",
    'Access-Control-Allow-Origin' : '*'
  })

  loginByEmail(form:LoginInterface):Observable<ResponseInterface>{
    let direccion = this.url + "login";
    return this.http.post<ResponseInterface>(direccion, form);
  }

  getVendedores(token:Object):Observable<ResponseInterface>{
    let direccion = this.url2 + "get-vendedores";
    return this.http.post<ResponseInterface>(direccion,token);
  }

  getClientes(token:Object):Observable<ResponseInterface>{
    let direccion = this.url3 + "cliente/get-cliente";
    return this.http.post<ResponseInterface>(direccion,token);
  }

  getProductos(token:Object):Observable<ResponseInterface>{
    let direccion = this.url3 + "producto/get-producto";
    return this.http.post<ResponseInterface>(direccion,token);
  }

}
